---
title: {{.Type}}{{if .Action.Deprecated}} (deprecated){{end}}
---

{{if .Action.Deprecated}}{{ "{{" }}<hint warning>{{ "}}" }}{{.Type}} version {{$.Version}} has been deprecated and will be removed in the future. A newer version may be available{{ "{{" }}</hint>{{ "}}" }}{{end}}

{{.Action.Doc}}

{{if gt ($.Example | len) 0}}
{{ "{{" }}<hint info>{{ "}}" }}auto-generated example may not be a valid request. See
[`{{$.Version}}.{{$.Action.Request}}`](/protocol/structures/{{$.Version}}/{{$.Action.Request}}/)
for all fields.{{ "{{" }}</hint>{{ "}}" }}
```json
{{$.Example}}
```
{{end}}

{{if ne $.Action.Request ""}}* Request Type: [`{{$.Version}}.{{$.Action.Request}}`](/protocol/structures/{{$.Version}}/{{$.Action.Request}}/){{end}}
{{if ne $.Action.Response ""}}* Response Type: {{if eq $.Action.Response "String"}}`string`{{else}}[`{{$.Version}}.{{$.Action.Response}}`](/protocol/structures/{{$.Version}}/{{$.Action.Response}}/){{end}}{{end}}

