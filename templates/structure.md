---
title: {{.Name}}
---

{{if ne .Version "v0"}}[[source code](https://gitlab.com/signald/signald/-/blob/main/src/main/java/io/finn/signald/clientprotocol/{{.Version}}/{{.Name}}.java)]{{end}}

{{.Type.Doc}}

This type can contain the following fields:

| field | type | info |
|-------|------|------|
{{range $name, $field:= .Type.Fields}}| `{{$name}}` |  {{if eq $field.List true}}List of {{end}}{{if ne $field.Version ""}}[`{{$field.Version}}.{{end}}{{$field.Type}}{{if ne $field.Version ""}}`](/protocol/structures/{{$field.Version}}/{{$field.Type}}/){{end}} | {{$field.Doc}} |
{{end}}

{{if gt (.References | len) 0}}
## References
{{range $ref := .References}}{{range $fieldName := $ref.Fields}}* [`{{$ref.Version}}.{{$ref.Name}}`](/protocol/structures/{{$ref.Version}}/{{$ref.Name}}/).{{$fieldName}}
{{end}}{{end}}
{{end}}