---
title: Servers
---

signald can use any server desired. Signal's production and staging servers are pre-loaded, but additional servers can be added via the client protocol.
The v1 [`generate_linking_uri`](/protocol/actions/v1/generate_linking_uri/) and [`register`](/protocol/actions/v1/register/) request types have a `server`
field, which (if set) will be used to select a server by UUID.

# Pre-Loaded Servers

These servers come pre-loaded in signald:

* **Signal Production** (`6e2eb5a8-5706-45d0-8377-127a816411a4`) *default unless changed at build time*
* **Signal Staging** (`97c17f0c-e53b-426f-8ffa-c052d4183f83`) preferred for testing. Signal states that it has lower rate limits.

# Custom Servers

There are two ways to add a new server:

## Socket Protocol

[`add_server`](/protocol/actions/v1/add_server/) can be used to add new servers via the socket protocol. Additionally, [`get_servers`](/protocol/actions/v1/get_servers/)
and [`delete_server`](/protocol/actions/v1/delete_server/) can be used to list and delete servers (including the preloaded ones).

## Compile Time Configuration

All of the server parameters can also be specified during compile time by setting environment variables. The full list can be found
[in `build.gradle`](https://gitlab.com/signald/signald/-/blob/main/build.gradle#L143-155). Make sure to set environment variable
`DEFAULT_SERVER_UUID` to a UUID that is not currently in use by any server. The build time server will only be used if the specified
UUID (or `DEFAULT_SERVER_UUID` if unspecified) is not found in the database. When that happens, the values will be copied into the database.