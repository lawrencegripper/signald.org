This article assumes that signald is installed and running. [click here for installation instructions](/articles/install/).

## Select a client

Users that have not selected a signald client will want to consult the [list of clients](/articles/clients/). Some clients have
their own documentation that includes account creation. This documentation primarily demonstrates how to use [signaldctl](/signaldctl]),
which may be needed in addition to whatever client you're using if your client doesn't guide you through registration.

## Account Creation

signald accounts can be created two different ways:
* linking to an existing Signal account (like the official Signal Desktop client)
* registering a new Signal account with a phone number you can receive SMS or voice calls to (like the official Signal mobile
clients)

Choose the one that works best for you and proceed:

{{< columns >}}
### Linking

To link to an existing Signal account:

```
signaldctl account link
```
a QR code will be displayed on screen. In the Signal app on your mobile device navigate to
[Settings](https://support.signal.org/hc/en-us/articles/360009091591), select linked devices and select the + (Android) or
"Link New Device" (iOS). Scan the code with the mobile device and wait a few seconds. When signaldctl exits your
account has been set up.

<--->
### Registering

To register a new account, first request an SMS verification code:

```
signaldctl account register [phone number]
```

{{<hint info>}}
You may need to complete a [captcha](/articles/captcha/) and pass the token with the `--captcha` flag
{{</hint>}}

{{<hint info>}}
A voice verification code may also be requested with the `--voice` flag.
{{</hint>}}

The phone number must be provided in [E.164](https://en.wikipedia.org/wiki/E.164) format, starting with a `+`. Once verification code has been received, submit it to
complete account setup:
```
signaldctl account verify [phone number] [code]
```
the code may be submitted with or without the `-` as it is formatted in SMS.
{{</columns>}}

{{<hint warning>}}

After setting up your account, you probably want to tell signaldctl to use it by default:

```
signaldctl config set defaultaccount +12024561414
```
{{</hint>}}

## Message Sending

### To Another User

To send a message from local account `+12025555555` to `+12026666666`:

{{<tabs "send-dm">}}
{{<tab "signaldctl">}}
```
signaldctl message send -a +12025555555 +12026666666 "hello, joe"
```

[documentation for `signaldctl message send`](/signaldctl/signaldctl_message_send/)
{{</tab>}}
{{<tab "signald-go">}}
```go
req := v1.SendRequest{
        Username:    "+12025555555",
        MessageBody: "hello, joe",
        RecipientAddress: &v1.JsonAddress{Number: "+12026666666"},
}

resp, err := req.Submit(signaldConn)
```

[documentation for v1.SendRequest](https://pkg.go.dev/gitlab.com/signald/signald-go/signald/client-protocol/v1#SendRequest)
{{</tab>}}
{{<tab "raw">}}
```json
{
    "username": "+12025555555",
    "recipientAddress": {"number": "+12026666666"},
    "messageBody": "hello, joe"
}
```

[documentation for v1 `send` request](/protocol/actions/v1/send/)
{{</tab>}}
{{</tabs>}}

### To A Group

To send a message from local account `+12025555555` to group `EdSqI90cS0UomDpgUXOlCoObWvQOXlH5G3Z2d3f4ayE=`:

{{<tabs "send-group">}}
{{<tab "signaldctl">}}
```
signaldctl message send -a +12025555555 EdSqI90cS0UomDpgUXOlCoObWvQOXlH5G3Z2d3f4ayE= "hello, everyone"
```

[documentation for `signaldctl message send`](/signaldctl/signaldctl_message_send/)
{{</tab>}}
{{<tab "signald-go">}}
```go
req := v1.SendRequest{
        Username:    "+12025555555",
        MessageBody: "hello, everyone",
        RecipientGroupID: "EdSqI90cS0UomDpgUXOlCoObWvQOXlH5G3Z2d3f4ayE=",
}

resp, err := req.Submit(signaldConn)
```

[documentation for v1.SendRequest](https://pkg.go.dev/gitlab.com/signald/signald-go/signald/client-protocol/v1#SendRequest)
{{</tab>}}
{{<tab "raw">}}
```json
{
    "username": "+12025555555",
    "recipientGroupId": "EdSqI90cS0UomDpgUXOlCoObWvQOXlH5G3Z2d3f4ayE=",
    "messageBody": "hello, everyone"
}
```

[documentation for v1 `send` request](/protocol/actions/v1/send/)
{{</tab>}}
{{</tabs>}}