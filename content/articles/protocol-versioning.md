---
title: Protocol Versioning
---

In order to allow for protocol changes without breaking backwards compatibility, signald request types have a version associated
with them. This is specified with the `"version"` key in the JSON request:

```json
{"type": "example", "version": "v1"}
```

Multiple versions of the same request type may exist at the same time, to allow clients time to update when things change in a
way that cannot be made backwards compatible.

All request types before this change have been dubbed `v0`. When a request with no version comes in, signald will print a
debug-level log line will be printed (run signald with `-v` to see these lines)

[almost all of the v0 protocol](https://gitlab.com/signald/signald/-/issues/88) has been replicated in v1. Although existing
clients do not need to update at this time, new clients should try to use v1+ requests. Internally, the request handling process
for v1+ is much more robust and has better error messaging (although not great, see [#149](https://gitlab.com/signald/signald/-/issues/149)).
Additionally, v1+ requests, responses and associated data types are available via the [machine-readable protocol documentation](/articles/protocol-documentation/),
and documented on this site (see Protocol header in the sidebar). v0 request types are not officially documented anywhere other
than the [README](https://gitlab.com/signald/signald/-/blob/main/README.md#socket-protocol).

Work on this protocol is ongoing. Feedback should be directed to [the issue tracker](https://gitlab.com/signald/signald/-/issues) or [IRC](/articles/IRC/)

## Deprecation

Eventually old versions of request types will be removed. When signald receives a request with a deprecated version of that request
type, a warn-level log line will be emitted.

* `alpha` versions may be removed within one minor version of being deprecated (that is, versions with the word `alpha` in the name, such as `v1alpha`)
* major versions (with no alpha or beta suffix) will be kept around for 6 months to a year after they are deprecated. Exact timeline will be announced on an individual basis.
* once all functionality in the `v0` is available in new style requests ([#88](https://gitlab.com/signald/signald/-/issues/88)), all `v0` requests will be deprecated together. They
    will remain in signald through the end of 2021, after which v0 functionality will be removed.