---
title: Captchas
---

Sometimes Signal requires a completion of a captcha to register. When this happens, signald throws a
`CaptchaRequiredException` error. If that error is thrown the user must complete a captcha.

## Getting A Token

Either use the [captcha helper utility](https://gitlab.com/signald/captcha-helper) or follow
these steps in your browser:

1. Go to https://signalcaptchas.org/registration/generate.html
1. Do whatever is needed to pass the captcha. You need to be able to load a Google captcha, which may
require disabling some content filtering. Try using a private browsing window if you get a blank screen.
1. You will be redirected to a URL that your browser won't understand, starting with `signalcaptcha://`.
Everything after `signalcaptcha://` is the captcha token, copy it. In Firefox, you may need to open the
Developer Console (Menu -> Web Developer -> Console or ctrl-shift-k), where the `signalcaptcha://` URL
will show up as part of a warning message.

## Register With A Token

Most users will simply need to provide the captcha token to their client. Client authors should simply
include the captcha token in a registration request.

{{<tabs "register">}}
{{<tab "signaldctl">}}
```
signaldctl account register +12024561414 --captcha 03AOLTBLR84zMWX9mh1gHaFZJwLYflPh0Bsi3_oYwsxJ9bTt_dV9mcmOMmhHZ19E_4waszAMc7EmPM7IfGSJc4471E45JLXgr2YjRlp36k7_AU5t8ww1IOrZid8hl9fqMs9FNIWx9IUj-TpmTdGnYTKpHhLKsQ5EjO53DeJcccp3Ay66PsvHWHXdda9rEAD-DDt6WbU7m-Mki_sVBIo3kJiV094fLOALTz7tTccAyGHH-rna9lIqceaxgeuvJhxteT_xdf2OU3df1TIQsUGbComAEYNCoreElcpBIv70BmsfCQiN_uUA76ILbnFJS5Ox_kFjqSscjR9V0VUbKE3SKknCc84NT3bPCYo9zSCUZDZ5VksbcaGxfziUMBnXG3MGUH__zu9hRoyZcf1ZWO-1_T2WnCQSvafKjle3DB08KBk5immiXwcIjkEDqshoJWXHUpHC7PcI0d0Q9hAXffuWvB4K5bfqNlgJIa_IigN_f06362sWyzHBkqcpcvAmJbf8rggKuW3OIQ5fvOIvUZ5NKBgZSj8g46mAoJj3fsLVCRn9vz7zKFZZRLMg8
```
{{</tab>}}
{{<tab "raw">}}
```json
{
    "type": "register",
    "version": "v1",
    "account": "+12024561414",
    "captcha": "03AOLTBLR84zMWX9mh1gHaFZJwLYflPh0Bsi3_oYwsxJ9bTt_dV9mcmOMmhHZ19E_4waszAMc7EmPM7IfGSJc4471E45JLXgr2YjRlp36k7_AU5t8ww1IOrZid8hl9fqMs9FNIWx9IUj-TpmTdGnYTKpHhLKsQ5EjO53DeJcccp3Ay66PsvHWHXdda9rEAD-DDt6WbU7m-Mki_sVBIo3kJiV094fLOALTz7tTccAyGHH-rna9lIqceaxgeuvJhxteT_xdf2OU3df1TIQsUGbComAEYNCoreElcpBIv70BmsfCQiN_uUA76ILbnFJS5Ox_kFjqSscjR9V0VUbKE3SKknCc84NT3bPCYo9zSCUZDZ5VksbcaGxfziUMBnXG3MGUH__zu9hRoyZcf1ZWO-1_T2WnCQSvafKjle3DB08KBk5immiXwcIjkEDqshoJWXHUpHC7PcI0d0Q9hAXffuWvB4K5bfqNlgJIa_IigN_f06362sWyzHBkqcpcvAmJbf8rggKuW3OIQ5fvOIvUZ5NKBgZSj8g46mAoJj3fsLVCRn9vz7zKFZZRLMg8"
}
```
{{</tab>}}
{{</tabs>}}

If you're having issues, [file an issue](https://gitlab.com/signald/signald/-/issues/new)