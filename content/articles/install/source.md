# Installation from source

1. Install Java (tested against Java 8 and 12)
1. Clone the signald repository:

    ```shell
    git clone https://gitlab.com/signald/signald.git
    ```

1. Build signald:

   ```shell
   cd signald
   make installDist
   ```

1. Configure the system directories:

   ```
   make setup
   ``` 

1. Start signald:

   ```shell
   build/install/signald/bin/signald
   ```

   It will continue running until killed (or <kbd>Ctrl-C</kbd>).

## Additional tooling

Consider installing [signaldctl](/signaldctl/).
