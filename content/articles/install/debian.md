# Debian Installation

*note that the debian repo is arm64 and amd64, 32 bit arm not currently supported ([#130](https://gitlab.com/signald/signald/-/issues/130))*

put the following in `/etc/apt/sources.list.d/signald.list`:

```
deb https://updates.signald.org unstable main
```

And trust the signing key:

```
curl https://updates.signald.org/apt-signing-key.asc | sudo apt-key add -
```

Update the package list:

```
sudo apt update
```

Now you can install signald:

```
sudo apt install signald
```

verify that the installation was successful:

```
signaldctl version
```